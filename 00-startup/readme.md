# K8S-Escape


## El Juego

Se inicia por el nivel 1, el mismo se ecuentra en el namespace "nivel-1".

En cada nivel se encontrará una password.

Con dicha password se deberá acceder a la URL https://sendati-training.gitlab.io/k8s-escape/{password}

Donde debe reemplazar **{password}** por la password encontrada.

Por ejemplo para las password **1234567890ABCDEF** la url es https://sendati-training.gitlab.io/k8s-escape/1234567890ABCDEF

En dicha pagina Web encontrará la pista para resolver el próximo nivel.


### Pre-Requisitos:

1. Instalar binario de kubectl (https://kubernetes.io/docs/tasks/tools/)

2. Obtener acceso a un cluster de Kubernetes o instalar minikube (https://minikube.sigs.k8s.io/docs/start/)

**Todos los ejemplos serán para un cluster de minikube en versión v1.23.1**

~~~
$ kubectl get nodes
NAME       STATUS   ROLES                  AGE     VERSION
minikube   Ready    control-plane,master   8m39s   v1.23.1
$
~~~

3. Validar que se tiene acceso de cluster-admin o similar

~~~
$ kubectl auth can-i delete node/minikube
Warning: resource 'nodes' is not namespace scoped
yes
$
~~~

4. Aplicar todos los YAMLs de los niveles al cluster

~~~
$ for file in $(ls -1 *.yaml); do kubectl apply -f $file ; done
namespace/level-1 created
namespace/level-2 created
.....
namespace/level-23 created
$ 
~~~

## Inicio del Juego

La pista para encontrar la password para acceder a la pista del nivel 2 es:

~~~
Encuentra el pod y su nombre te indicará la password.
~~~

